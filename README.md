### Build notes:

1. On Mac OS X it won't work unless there is a shell prepared for this purpose:
> boot2docker init &&
> boot2docker start &&
> $(boot2docker shellinit 2> /dev/null)
2. To build a Docker container:
> docker build -t mysitename . &&
> docker run -p 8080:80 -d mysitename
3. Container is now initialised at the ip provided by: boot2docker ip